import os

try:  # for pip >= 10
    from pip._internal.req import parse_requirements
    from pip._internal.download import PipSession
except ImportError:  # for pip <= 9.0.3
    from pip.req import parse_requirements
    from pip.download import PipSession
from setuptools import setup, find_packages


def read_requirements():
    """
    parses requirements from requirements.txt
    :return: List of requirements
    """
    requirements_path = os.path.join(os.path.dirname(__file__), 'requirements.txt')
    install_requirements = parse_requirements(requirements_path, session=PipSession())
    requirements = [str(ir.req) for ir in install_requirements]
    return requirements


setup(
    name='zeus',
    version='0.1.0',
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    install_requires=read_requirements(),
    url='https://bitbucket.org/truedmp/zeus.git',
    license='',
    author='Phasathorn Suwansri',
    author_email='phasathorn.suwansri@truedigital.com',
    description='Classifier and Standardizer'
)
