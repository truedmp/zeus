import re
from typing import Dict, List, Union


class Parser(object):
    def __init__(self, dictionary: Dict[str, Union[List[str], str]]) -> None:
        self.dictionary = dictionary

    def __call__(self, items):
        raise NotImplementedError


class StandardFormParser(Parser):
    def __init__(self, dictionary: Dict[str, str]):
        super().__init__(dictionary)

    def _parse_list(self, items: List[str]) -> List[str]:
        parsed_items = set()
        for standard_form, trie_pattern in self.dictionary.items():
            for item in items:
                if re.search(trie_pattern, item):
                    parsed_items.add(standard_form)
        return list(parsed_items)

    def _parse_string(self, string: str) -> List[str]:
        parsed_items = set()
        for standard_form, trie_pattern in self.dictionary.items():
            if re.search(trie_pattern, string):
                parsed_items.add(standard_form)
        return list(parsed_items)

    def __call__(self, item: Union[List[str], str]) -> List[str]:
        if isinstance(item, str):
            return self._parse_string(item)
        elif isinstance(item, list):
            return self._parse_list(item)
        else:
            raise TypeError(f"type {type(item)} is not supported")


class HierarchicalFormParser(Parser):
    def __init__(self, dictionary: Dict[str, List[str]]):
        super().__init__(dictionary)

    def _parse(self, items: List[str]) -> List[str]:
        parsed_items = []
        for item in items:
            hierarchical_forms = self.dictionary.get(item, [])
            parsed_items.extend(hierarchical_forms)
        return list(set(parsed_items))

    def __call__(self, items: List[str]) -> List[str]:
        return self._parse(items)
