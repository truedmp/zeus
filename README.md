# ZEUS

ZEUS project use to classify and standardize data

## Current version

0.1.0

## Release detail

0.1.0

- Support StandardForm and HierarchicalForm Parser from dictionary
- Support data type `string` and `list`

---

# Index

- [Parser](##Parse)
    - [StandardFormParser](###StandardFormParser)
    - HierarchicalFormParser
 

## Parser

### StandardFormParser


#### How to use

```python
import pandas as pd
from zeus.models.parser import StandardFormParser

# Load dictionary from zeus resources
standard_form_dict_json = {
    "sushi": "(?:sushi|ซูชิ)"
}

dataframe = pd.DataFrame([{
    "tag": ["ซูชิ", "ignored"],
    "string": "sushi"
}])
"""
| string     | tag                  |
|------------|----------------------|
| sushi      | ["ซูชิ", "ignored"]    |
"""

standard_form_parser = StandardFormParser(dictionary=standard_form_dict_json)
dataframe["standard_form_tag"] = dataframe.tag.map(standard_form_parser)

"""
Output:
| string     | tag                  | standard_form_tag |
|------------|----------------------|-------------------|
| sushi      | ["ซูชิ", "ignored"]    | ["sushi"]         |
"""

```


#### How to use with dictionary file


```python
import json

from zeus.models.parser import StandardFormParser

# Load dictionary from zeus resources

with open("standard_form.json") as f:
    standard_form_dict_json = json.loads(f.read())
"""
| string     | tag                  |
|------------|----------------------|
| sushi      | ["ซูชิ", "ignored"]    |
"""


standard_form_parser = StandardFormParser(dictionary=standard_form_dict_json)
dataframe["standard_form_tag"] = dataframe.tag.map(standard_form_parser)

"""
Output:
| string     | tag                  | standard_form_tag |
|------------|----------------------|-------------------|
| sushi      | ["ซูชิ", "ignored"]    | ["sushi"]         |
"""

```



#### How to use with DataX


```python
from datax import GCS
from datax.helpers.google_storage_client import get_storage_client
 
from zeus.models.parser import StandardFormParser

# Load dictionary from zeus resources
def get_standard_form_dict(bucket, path):
    gcs = GCS()
    dtx = gcs.download(bucket, path)
    return dtx.df
    
client = get_storage_client(project="true-dmp", credentials_path="./TRUE-DMP-89xxx.json")
bucket = client.get_bucket("dmp_recommendation_dev") 
standard_form_dict_json = get_standard_form_dict(bucket, "resources/knowledge_base/zeus/standard_form.json")

"""
| string     | tag                  |
|------------|----------------------|
| sushi      | ["ซูชิ", "ignored"]    |
"""

standard_form_parser = StandardFormParser(dictionary=standard_form_dict_json)
dataframe["standard_form_tag"] = dataframe.tag.map(standard_form_parser)

"""
Output:
| string     | tag                  | standard_form_tag |
|------------|----------------------|-------------------|
| sushi      | ["ซูชิ", "ignored"]    | ["sushi"]         |
"""

```