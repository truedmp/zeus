from unittest import TestCase

import pandas as pd

from zeus.models.parser import HierarchicalFormParser


class TestHierarchicalFormParser(TestCase):
    def setUp(self):
        self.dictionary = {
            "food-beverage-related": [
                "1_food-beverage-related"
            ],
            "food": [
                "2_food",
                "1_food-beverage-related"
            ],
            "food-japanese": [
                "3_food-japanese",
                "2_food",
                "1_food-beverage-related"
            ],
            "sushi": [
                "4_sushi",
                "3_food-japanese",
                "2_food",
                "1_food-beverage-related"
            ],
            "ramen": [
                "4_ramen",
                "3_food-japanese",
                "2_food",
                "1_food-beverage-related"
            ]
        }
        self.df = pd.DataFrame([{
            "tag": ["sushi", "food"]
        }])

    def test__parse(self):
        hfp = HierarchicalFormParser(self.dictionary)
        actual = self.df.tag.map(hfp)
        actual = sorted(actual.values[0])
        expected = ["4_sushi", "3_food-japanese", "2_food", "1_food-beverage-related"]
        expected = sorted(expected)
        self.assertEqual(expected, actual)
