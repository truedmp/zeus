from unittest import TestCase

import pandas as pd

from zeus.models.parser import StandardFormParser


class TestStandardFormParser(TestCase):
    def setUp(self):
        self.dictionary = {
            "sushi": "(?:sushi|ซูชิ)",
            "ramen": "(?:ramen|ราเม[งน])"
        }
        self.df = pd.DataFrame([{
            "tag": ["ramen"],
            "string": "ราเมง"
        }, {
            "tag": ["ราเมน", "ราเมง"],
            "string": "sushi"
        }])

    def test__parse_list(self):
        sfp = StandardFormParser(self.dictionary)
        actual = self.df.tag.map(sfp)
        self.assertEqual([["ramen"], ['ramen']], list(actual))

    def test__parse_string(self):
        sfp = StandardFormParser(self.dictionary)
        actual = self.df.string.map(sfp)
        self.assertEqual([["ramen"], ['sushi']], list(actual))
